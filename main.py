#!/usr/bin/env python3

import smtpd
import asyncore
from email import message_from_bytes
import email.policy
from dateutil.parser import parse
import subprocess
import re

class CustomSMTPServer(smtpd.SMTPServer):
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        msg = message_from_bytes(data, policy=email.policy.default)
        body = msg.get_content()
        d = parse(msg.get('Date'))
        title = msg.get('Subject')

        if not title:
            print("Title missing")
            return

        valid_destinations = [
                'massutskick@vastgota.nation.liu.se',
                'massutskick@vastgota.lysator.liu.se',
                ]

        for destination in valid_destinations:
            if destination in msg.get('To'):
                break
        else:
            print('Will only publish massutskick')
            return

        title = re.sub('^\[.*\] *', '', title)

        subprocess.run(['wp', 'post', 'create',
                '--post_author=2'
                f'--post_date={d:%Y-%m-%d}',
                f"--post_title={title}",
                '--post_status=publish',
                '--post_category=Veckomail',
                f"--post_content=<pre>{body}</pre>"],
                cwd='/usr/share/wordpress',
                shell=False)
        return


server = CustomSMTPServer(('0.0.0.0', 1025), None,
        decode_data=False)
asyncore.loop()
